/*
 * Sample program for DocodeModem
 * receive only
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <docodemo.h>
#include <SlrModem.h>

#define SEND_PERIOD_MS 5000

const uint8_t CHANNEL = 0x10;   //10進で16チャネルです。通信相手と異なると通信できません。
const uint8_t DEVICE_DI = 0x00; //通信相手のIDです。0は全てに届きます。
const uint8_t DEVICE_EI = 0x1;  // 自分のIDです
const uint8_t DEVICE_GI = 0x02; //グループIDです。通信相手と異なると通信できません。

DOCODEMO Dm = DOCODEMO();
SlrModem modem;

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  uint8_t recvdata[255];

  Dm.begin(); //初期化が必要です。

  //modem uart
  UartModem.begin(MLR_BAUDRATE);

  //モデムの電源を入れて少し待ちます
  Dm.ModemPowerCtrl(ON);
  delay(150);

  //モデム操作用に初期化します
  modem.Init(UartModem, nullptr);

  //各無線設定を行います。電源入り切りするようであればtrueにして内蔵Flashに保存するようにしてください。
  modem.SetMode(SlrModemMode::LoRaCmd, false);
  modem.SetChannel(CHANNEL, false);
  modem.SetDestinationID(DEVICE_DI, false);
  modem.SetEquipmentID(DEVICE_EI, false);
  modem.SetGroupID(DEVICE_GI, false);

  while (1)
  {
    modem.Work(); //ループ処理。受信データを処理しています。

    if (modem.HasPacket())
    {
      const uint8_t *pData;
      uint8_t len{0};

      //受信データのポインタとサイズを取得
      modem.GetPacket(&pData, &len);

      //受信データ取得
      memcpy(&recvdata[0], pData, len);

      //受信データ出力。ここを必要な処理に変更してください。
      SerialDebug.write(recvdata, len);

      //受信データ開放
      modem.DeletePacket();
    }

    delay(10);
  }
}

void setup()
{
  SerialDebug.begin(115200);
  vSetErrorSerial(&SerialDebug);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error scheduler failed to start
  // should never get here
  while (1)
  {
    SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{

}